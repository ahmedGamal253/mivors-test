//
//  PromotionesResponseModel.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/25/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import Foundation
class PromotionesResponseModel{
    
    var Promotions = [PromotionModel]()
    
    init (_ response: [String: Any]){
        for dic in response["Promotions"] as? [[String: Any]] ?? [[String: Any]]() {
            let model = PromotionModel(dic)
            Promotions.append(model)
        }
    }
}
