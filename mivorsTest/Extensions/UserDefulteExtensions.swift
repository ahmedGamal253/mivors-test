//
//  UserDefulteExtensions.swift
//  mivorsTest
//
//  Created by jimmy on 2/23/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import Foundation

extension UserDefaults{
    
    func isUser()-> Bool{
        UserDefaults.standard.object(forKey: "user_id") != nil ? true : false
    }
    
    func setUserId(id:String){
        UserDefaults.standard.set(id, forKey:"user_id")
    }
    
    func removeUserId(){
        UserDefaults.standard.removeObject(forKey: "user_id")
    }
}
