//
//  UIButtonExtensio.swift
//  nasTrends
//
//  Created by Mohamed Elmaazy on 1/23/19.
//  Copyright © 2019 Mohamed Elmaazy. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    func setFont(name: String, size: CGFloat) {
        self.titleLabel?.font = UIFont(name: name, size: (size) / 360 * UIScreen.main.bounds.width)!
    }

}
