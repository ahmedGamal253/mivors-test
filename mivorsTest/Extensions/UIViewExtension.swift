//
//  UIViewExtension.swift
//  Pharmacist
//
//  Created by mohamed elmaazy on 7/9/18.
//  Copyright © 2018 Ahmed gamal. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func setBorder(color:UIColor, radius:CGFloat, borderWidth:CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = borderWidth
    }
    
    func setBorder(color:CGColor, radius:CGFloat, borderWidth:CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.borderColor = color
        self.layer.borderWidth = borderWidth
    }
    
    
    func shakeWithError(color:UIColor) {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 3
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 10, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 10, y: self.center.y))
        self.layer.add(animation, forKey: "position")
        
        self.setBorder(color: .red, radius: 5, borderWidth: 1)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
            self.setBorder(color: color, radius: 5, borderWidth: 1)
        }
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
}
