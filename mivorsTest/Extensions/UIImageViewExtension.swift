//
//  Gif.swift
//  SwiftGif
//
//  Created by Arne Bahlo on 07.06.14.
//  Copyright (c) 2014 Arne Bahlo. All rights reserved.
//

import UIKit
import ImageIO
import Kingfisher

extension UIImageView {
    public func loadFromUrl(url:String) {
        let url = URL(string: url)
        self.kf.indicatorType = .activity
        self.kf.setImage(with: url, placeholder:UIImage.init(named: "logoBress"), options: nil, progressBlock: nil, completionHandler: nil)
    }
}

extension UIImage{
    func getThumbnial() -> UIImage {
        var width:CGFloat = 0.0
        var height:CGFloat = 0.0
        if self.size.width > 1024 {
            width = 200
            let ratio = self.size.width / width
            height = self.size.height / ratio
        } else {
            width = self.size.width
            height = self.size.height
        }
        let originalImage = self
        let destinationSize = CGSize.init(width: width, height: height)
        UIGraphicsBeginImageContext(destinationSize)
        originalImage.draw(in: CGRect.init(x: 0, y: 0, width: destinationSize.width, height: destinationSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
