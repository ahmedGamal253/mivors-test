//
//  UIViewControllerExtension.swift
//  Pharmacist
//
//  Created by mohamed elmaazy on 7/9/18.
//  Copyright © 2018 Ahmed gamal. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

extension UIViewController: UIGestureRecognizerDelegate {

    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    func showAlart(message:String){
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: OK.localize, style: .default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func getRatio(_ number: CGFloat) -> CGFloat {
        let ratio = CGFloat(Int(number * UIScreen.main.bounds.width / 360))
        return ratio > 0 ? ratio : CGFloat(number * UIScreen.main.bounds.width / 360)
    }
    
    func animateView(){
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
}

extension UIViewController: NVActivityIndicatorViewable {
    
    func showIndicator() {
        let size = CGSize(width: getRatio(70), height: getRatio(70))
        startAnimating (size , type: .ballScaleRippleMultiple, color: .white , padding: 10 )
    }
    
    func hideIndicator(){
        stopAnimating()
    }
}
