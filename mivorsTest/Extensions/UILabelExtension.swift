//
//  UILabelExtension.swift
//  Safir El3rood
//
//  Created by mohamed elmaazy on 7/26/18.
//  Copyright © 2018 Internet Plus. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    func setFont(name: String, size: CGFloat) {
        self.font = UIFont(name: name, size: (size) / 360 * UIScreen.main.bounds.width)!
    }
    
}
