//
//  UITextFieldExtention.swift
//  Pharmacist
//
//  Created by Ahmed gamal on 7/18/18.
//  Copyright © 2018 Ahmed gamal. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with:self.text!)
    }
    
    func isValidMobileNumber() -> Bool {
        if self.text!.count == 11 &&  self.text?.prefix(2) == "01"{
            return true
        } else{
            return false
        }
    }
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
    func setFont(name: String, size: CGFloat) {
        self.font = UIFont(name: name, size: (size) / 360 * UIScreen.main.bounds.width)!
    }
}
