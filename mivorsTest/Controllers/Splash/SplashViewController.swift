//
//  ViewController.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/22/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    let viewModel = SplashViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setTimer()
    }
    
    private func setTimer(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1 , execute: {
            self.initView()
        })
    }
    
    private func initView(){
        _ = viewModel.openHome.subscribe(onNext: { (open) in
            if open {
                self.pushNavWithRoot(root: HomeViewController())
            }
        })
        _ = viewModel.openLogin.subscribe(onNext: { (open) in
            if open {
                self.pushNavWithRoot(root: LoginStepsViewController())
            }
        })
    }
    
    private func pushNavWithRoot(root:UIViewController){
        let nav = UINavigationController(rootViewController: root)
        nav.modalPresentationStyle = .fullScreen
        nav.isNavigationBarHidden = true
        UIView.appearance().semanticContentAttribute = AppLanguageHandler.instance.getLanguage() == ENGLISH ? .unspecified : .forceRightToLeft
        self.present(nav, animated: true, completion: nil)
    }
}

