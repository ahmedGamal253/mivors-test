//
//  SplashViewModel.swift
//  mivorsTest
//
//  Created by jimmy on 2/23/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class SplashViewModel{
    var openHome = BehaviorRelay(value: false)
    var openLogin = BehaviorRelay(value: false)
    
    init() {
        checkUser()
    }
    
  private  func checkUser(){
        UserDefaults.standard.isUser() ? (openHome.accept(true)) : (openLogin.accept(true))
    }
    
}
