//
//  PromotionViewModel.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/25/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import Foundation
import UIKit

class PromotionViewModel{
    
    var arrayTabsNames = [String]()
    var arrarTabs = [UIViewController]()
    
    init() {
        SetTabNames()
        setTabControllers()
    }
    
    private func SetTabNames(){
        arrayTabsNames.append(SERVICE.localize)
        arrayTabsNames.append(ACCESSORIES.localize)
        arrayTabsNames.append(PROMOTIONS.localize)
        arrayTabsNames.append(BRANCHES.localize)
    }
    
    private func setTabControllers(){
        arrarTabs.append(ServiceViewController())
        arrarTabs.append(EmptyViewController())
        arrarTabs.append(EmptyViewController())
        arrarTabs.append(EmptyViewController())
    }
}
