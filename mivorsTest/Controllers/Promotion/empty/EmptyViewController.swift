//
//  EmptyViewController.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/25/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import UIKit

class EmptyViewController: UIViewController {

    @IBOutlet weak var labelNotAvilable: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        labelNotAvilable.text = NOT_AVIALABLE_NOW.localize
        labelNotAvilable.setFont(name: AppFont.instance.regular, size: 17)
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
