//
//  ServiceViewController.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/25/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ServiceViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let viewModel = ServiceViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
        viewModel.getData()
        initRx()
        // Do any additional setup after loading the view.
    }
    
    private func initRx(){
        _ = viewModel.showError.subscribe(onNext: { (error) in
            self.showAlart(message: error)
        })
        _ = viewModel.reloadTable.subscribe({ (reload) in
            self.tableView.reloadData()
        })
        _ = viewModel.showLoader.subscribe(onNext: { (show) in
            show ?  self.showIndicator() : self.hideIndicator()
        })
    }

}
