//
//  ServiceViewModel.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/25/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import Foundation
 import RxSwift
 import RxCocoa

class ServiceViewModel{
    
    var showLoader = BehaviorRelay(value: false)
    var promotionsArray = [PromotionModel]()
    var reloadTable = BehaviorRelay(value: false)
    var showError = BehaviorRelay(value: "")
    
    func getData(){
        showLoader.accept(true)
        let instance = AppConnectionsHandler()
        instance.responseClosure = {(responseType: ResponseStatus, response:[String:Any]?) in
            self.showLoader.accept(false)
            switch responseType {
            case .sucess:
               let responseModel = PromotionesResponseModel(response!)
               self.promotionsArray = responseModel.Promotions
               self.reloadTable.accept(true)
                break
            case .errorFromServer:
                self.showError.accept(response!["Error"] as! String)
                break
            case .errorFromInternet:
                self.showError.accept(ERROR_INTERNET.localize)
                break
            }
        }
        instance.get(url:AppUrl.instance.promotions , headers: AppUtils.instance.getHeader())
    }
}
