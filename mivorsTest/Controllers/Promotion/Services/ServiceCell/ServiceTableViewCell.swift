//
//  ServiceTableViewCell.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/25/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import UIKit

class ServiceTableViewCell: UITableViewCell {

    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewNext: UIView!
    @IBOutlet weak var imageService: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setFonts()
        initUI()
        viewNext.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedNext)))
        // Initialization code
    }
    
    private func setFonts(){
        labelTitle.setFont(name: AppFont.instance.regular, size: 16)
        labelDescription.setFont(name: AppFont.instance.regular, size: 14)
       
    }
    
    private func initUI(){
        viewCell.layer.cornerRadius = 5
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.viewCell.roundCorners([.bottomRight], radius: 20)
            self.viewNext.layer.cornerRadius = 30
        })
    }
    
    func setData(_ model:PromotionModel){
      imageService.loadFromUrl(url: model.image)
      if AppLanguageHandler.instance.IsEnglish(){
            labelTitle.text = model.Title
            labelDescription.text = model.Description
        }else{
            labelTitle.text = model.Titlear
            labelDescription.text = model.Descriptionar
        }
    }
    
    @objc func tappedNext(){
        NotificationCenter.default.post(name: Notification.Name.openDetails, object: nil)
    }
}
