//
//  ServiceViewExtensions.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/25/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import Foundation
import UIKit

extension ServiceViewController: UITableViewDelegate, UITableViewDataSource {
    
    func initTableView() {
        let nib = UINib(nibName: "ServiceTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ServiceTableViewCell")
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.promotionsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String = "ServiceTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ServiceTableViewCell
        cell.selectionStyle = .none
        cell.setData(viewModel.promotionsArray[indexPath.row])
        return cell
    }
}
