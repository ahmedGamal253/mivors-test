//
//  PromotionViewController.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/25/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import UIKit
import BmoViewPager

class PromotionViewController: UIViewController {
    
    @IBOutlet weak var viewContentTabs: BmoViewPager!
    @IBOutlet weak var viewNavigationTabs: BmoViewPagerNavigationBar!
    
    let viewModel = PromotionViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        initTabs()
        initObserver()
        // Do any additional setup after loading the view.
    }
    
    private func initObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(openDetails), name:NSNotification.Name.openDetails, object: nil)
    }
    
    @objc private func openDetails(){
        showAlart(message: NOT_AVIALABLE_NOW.localize)
    }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
