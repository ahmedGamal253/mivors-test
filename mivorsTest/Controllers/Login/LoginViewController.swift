//
//  LoginViewController.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/22/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var btnFaceBook: UIButton!
    @IBOutlet weak var labelSocialAccount: UILabel!
    @IBOutlet weak var labelOrUsing: UILabel!
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var textPassword: LeftTextField!
    @IBOutlet weak var textEmail: LeftTextField!
    @IBOutlet weak var labelUsingApp: UILabel!
    @IBOutlet weak var labelContinu: UILabel!
    @IBOutlet weak var labelBack: UILabel!
    
    let appFont = AppFont.instance
    let viewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initRx()
        // Do any additional setup after loading the view.
    }

    private func initView(){
        DispatchQueue.main.async {
            self.initViewForLanguage()
            self.initFonts()
            self.initUi()
        }
    }
    
    private func initViewForLanguage(){
        labelBack.text = WELCOM_BACK.localize
        labelContinu.text = SIGN_IN_TO_CONTINU.localize
        labelUsingApp.text = USING_OUR_APP.localize
        btnSignIn.setTitle(LOGIN.localize, for: .normal)
        labelOrUsing.text = OR_LOGIN_SOCIAL.localize
        labelSocialAccount.text = SOCIAL_ACCOUNT.localize
        btnGoogle.setTitle(GOOGLE.localize, for: .normal)
        btnFaceBook.setTitle(FACEBOOK.localize, for: .normal)
        textEmail.placeholder = USER_NAME_OR_EMAIL.localize
        textPassword.placeholder = PASSWORD.localize
    }
    
    private func initFonts(){
        labelBack.setFont(name: appFont.bold, size: 33)
        labelContinu.setFont(name: appFont.regular, size: 16)
        labelUsingApp.setFont(name: appFont.regular, size: 16)
        btnSignIn.setFont(name: appFont.bold, size: 17)
        btnGoogle.setFont(name: appFont.bold, size: 17)
        btnFaceBook.setFont(name: appFont.bold, size: 17)
        labelOrUsing.setFont(name: appFont.regular, size: 16)
        labelSocialAccount.setFont(name: appFont.regular, size: 16)
        textPassword.setFont(name: appFont.regular, size: 16)
        textEmail.setFont(name: appFont.regular, size: 16)
    }
    
    private func initUi(){
        textEmail.placeHolderColor = #colorLiteral(red: 0.7058823529, green: 0.7450980392, blue: 1, alpha: 1)
        textPassword.placeHolderColor = #colorLiteral(red: 0.7058823529, green: 0.7450980392, blue: 1, alpha: 1)
        textEmail.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        textPassword.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        btnSignIn.layer.cornerRadius = 5
        btnGoogle.layer.cornerRadius = 5
        btnFaceBook.layer.cornerRadius = 5
    }
    
    private func initRx(){
        _ = viewModel.showError.subscribe(onNext: { (error) in
            self.showAlart(message: error)
        })
        _ = viewModel.showLoader.subscribe(onNext: { (show) in
            show ?  self.showIndicator() : self.hideIndicator()
        })
        _ = viewModel.sucsessLogin.subscribe(onNext: { (show) in
            self.navigationController?.pushViewController(HomeViewController(), animated: true)
        })
    }

    
    @IBAction func signIn(_ sender: Any) {
        viewModel.tappedSignIn(email: textEmail.text!, password: textPassword.text!)
    }
    
    @IBAction func signInGoogle(_ sender: Any) {
        viewModel.tappedGoogal()
    }
    
    @IBAction func signInFaceBook(_ sender: Any) {
        viewModel.tappedFacbook()
    }
}
