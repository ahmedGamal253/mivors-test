//
//  LoginViewModel.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/23/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class LoginViewModel{
    
    var showError = BehaviorRelay(value: "")
    var showLoader = BehaviorRelay(value: false)
    var sucsessLogin = BehaviorRelay(value: false)
    
    func tappedGoogal(){
        showNotAvailable()
    }
    
    func tappedFacbook(){
        showNotAvailable()
    }
    
    private func showNotAvailable(){
        showError.accept(NOT_AVIALABLE_NOW.localize)
    }
    
    func tappedSignIn(email:String,password:String){
        if  email.isEmpty{
            showError.accept(ENTER_VALID_EMAIL.localize)
        }else if password.isEmpty{
            showError.accept(ENTER_PASSWORD.localize)
        }else{
            makeSignIn(email: email,password: password)
        }
    }
    
    private func makeSignIn(email:String,password:String){
        showLoader.accept(true)
        let instance = AppConnectionsHandler()
        var params = [String:String]()
        params.updateValue(email, forKey: "Email")
        params.updateValue(password, forKey: "Password")
        instance.responseClosure = {(responseType: ResponseStatus, response: [String:Any]?)in
            self.showLoader.accept(false)
            switch responseType {
            case.sucess:
                UserDefaults.standard.setUserId(id: response!["ID"] as? String ?? "")
                self.sucsessLogin.accept(true)
                break
            case.errorFromServer:
                self.showError.accept(response!["Error"] as! String)
                break
            case.errorFromInternet:
                self.showError.accept(ERROR_INTERNET.localize)
                break
            }
            
        }
        instance.post(url: AppUrl.instance.login, params: params, headers: AppUtils.instance.getHeader())
    }
}
