//
//  ServiceAppointmentViewController.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/25/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import UIKit

class ServiceAppointmentViewController: UIViewController {
    
    @IBOutlet weak var labelMyCar: LeftLBL!
    @IBOutlet weak var btnBook: UIButton!
    @IBOutlet weak var textTime: LeftTextField!
    @IBOutlet weak var textDate: LeftTextField!
    @IBOutlet weak var viewWorkShop: UIView!
    @IBOutlet weak var textCity: LeftTextField!
    @IBOutlet weak var textServiceType: LeftTextField!
    @IBOutlet weak var viewMyCar: UIView!
    @IBOutlet weak var labeltitle: UILabel!
    @IBOutlet weak var labelWorkShop: UILabel!
    let appFont = AppFont.instance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        // Do any additional setup after loading the view.
    }
    
    private func initView(){
        DispatchQueue.main.async {
            self.initViewForLanguage()
            self.initFonts()
            self.initUi()
        }
    }
    
    private func initViewForLanguage(){
        labelMyCar.text = MY_CAR.localize
        labeltitle.text = SERVIVE_APPOINTMENT.localize
        textServiceType.placeholder = SERVICE_TYPE.localize
        textCity.placeholder = CITY.localize
        labelWorkShop.text = WORKSHOP.localize
        textDate.placeholder = DATE.localize
        textTime.placeholder = TIME.localize
        btnBook.setTitle(BOOK.localize, for: .normal)
    }
    
    private func initFonts(){
        labeltitle.setFont(name: appFont.bold, size: 17)
        labelMyCar.setFont(name: appFont.regular, size: 16)
        textServiceType.setFont(name: appFont.regular, size: 16)
        textCity.setFont(name: appFont.bold, size: 16)
        labelWorkShop.setFont(name: appFont.bold, size: 16)
        textDate.setFont(name: appFont.regular, size: 16)
        textTime.setFont(name: appFont.regular, size: 16)
        btnBook.setFont(name: appFont.bold, size: 17)
    }
    
    private func initUi(){
        textCity.placeHolderColor = #colorLiteral(red: 0.7058823529, green: 0.7450980392, blue: 1, alpha: 1)
        textServiceType.placeHolderColor = #colorLiteral(red: 0.7058823529, green: 0.7450980392, blue: 1, alpha: 1)
        textDate.placeHolderColor = #colorLiteral(red: 0.7058823529, green: 0.7450980392, blue: 1, alpha: 1)
        textTime.placeHolderColor = #colorLiteral(red: 0.7058823529, green: 0.7450980392, blue: 1, alpha: 1)
        viewMyCar.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        textServiceType.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        textCity.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        viewWorkShop.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        textDate.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        textTime.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        textCity.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        btnBook.layer.cornerRadius = 5
    }
    
    private func initGestures(){
        viewMyCar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedMyCar)))
        viewWorkShop.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedWorkshop)))
    }
    
    @objc private func tappedMyCar(){
        
    }
    
    @objc private func tappedWorkshop(){
        
    }
    
    @IBAction func BookAppointment(_ sender: Any) {
        
    }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
