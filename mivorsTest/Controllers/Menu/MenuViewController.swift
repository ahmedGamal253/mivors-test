//
//  MenuViewController.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/22/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    @IBOutlet weak var viewSign: UIView!
    @IBOutlet weak var labelSign: UILabel!
    @IBOutlet weak var viewIndex: UIView!
    @IBOutlet weak var viewHistory: UIView!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var viewLanguage: UIView!
    let languageHandler = AppLanguageHandler.instance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initGestures()
        initView()
        // Do any additional setup after loading the view.
    }

    private func initGestures(){
        viewLanguage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(changeLanguage)))
        viewSign.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedSign)))
    }
    
    private func initView(){
        if !UserDefaults.standard.isUser(){
            viewProfile.isHidden = true
            viewHistory.isHidden = true
            viewIndex.isHidden = true
            labelSign.text = LOGIN.localize
        }else{
            labelSign.text = LOGOUT.localize
        }
    }
    
   @objc func changeLanguage(_ sender: Any) {
      //  dropDown.show()
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: nil , message: nil, preferredStyle: .actionSheet)
        let cancelActionButton = UIAlertAction(title: CANCEL.localize, style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        let saveActionButton = UIAlertAction(title: "العربية", style: .default)
        { _ in
            if !(self.languageHandler.getLanguage() == ARABIC) {
                self.languageHandler.setLanguage(language: ARABIC)
                self.dismiss(animated: true, completion: {
                    NotificationCenter.default.post(name: Notification.Name.dismissNav, object: nil)
                })
            }
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "English", style: .default)
        { _ in
            if !(self.languageHandler.getLanguage() == ENGLISH) {
                self.languageHandler.setLanguage(language: ENGLISH)
                self.dismiss(animated: true, completion: {
                    NotificationCenter.default.post(name: Notification.Name.dismissNav, object: nil)
                })
            }
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    @objc func tappedSign(){
        if UserDefaults.standard.isUser(){
            UserDefaults.standard.removeUserId()
            dismiss(animated: true, completion: {
                 NotificationCenter.default.post(name: Notification.Name.dismissNav, object: nil)
            })
        }else{
            navigationController?.pushViewController(LoginStepsViewController(), animated: true)
        }
    }
}
