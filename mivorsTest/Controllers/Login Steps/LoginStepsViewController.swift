//
//  LoginStepsViewController.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/22/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import UIKit
import BmoViewPager

class LoginStepsViewController: UIViewController {

    @IBOutlet weak var labelSkip: UILabel!
    @IBOutlet weak var imageHeader: UIImageView!
    @IBOutlet weak var viewSkip: UIView!
    @IBOutlet weak var viewContentTabs: BmoViewPager!
    @IBOutlet weak var viewNavigationTabs: BmoViewPagerNavigationBar!
    
    var arrayTabsNames = [String]()
    var arrarTabs = [UIViewController]()
    var viewModel = LoginStepsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTabs()
        initGuestures()
        initView()
        // Do any additional setup after loading the view.
    }
    
    private func initGuestures(){
        viewSkip.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedSkip)))
    }

    @objc private func tappedSkip(){
        navigationController?.pushViewController(HomeViewController(), animated: true)
    }
    
    private func initView(){
        DispatchQueue.main.async {
            self.initViewForLanguage()
            self.initFonts()
        }
    }
    
    private func initViewForLanguage(){
        labelSkip.text = SKIP.localize
    }
    
    private func initFonts(){
        labelSkip.setFont(name: AppFont.instance.regular, size: 16)
    }
}
