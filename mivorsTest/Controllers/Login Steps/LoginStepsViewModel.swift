//
//  LoginStepsViewModel.swift
//  mivorsTest
//
//  Created by jimmy on 2/23/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import Foundation
import UIKit

class LoginStepsViewModel{
    var arrayTabsNames = [String]()
    var arrarTabs = [UIViewController]()
    
    init() {
        SetTabNames()
        setTabControllers()
    }
    
    private func SetTabNames(){
        arrayTabsNames.append(LOGIN.localize)
        arrayTabsNames.append(SIGN_UP.localize)
    }
    
    private func setTabControllers(){
        arrarTabs.append(LoginViewController())
        arrarTabs.append(RegisterViewController())
    }
    
    
}
