//
//  LoginStepsExtensions.swift
//  mivorsTest
//
//  Created by jimmy on 2/23/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import Foundation
import BmoViewPager

extension LoginStepsViewController: BmoViewPagerDataSource, BmoViewPagerDelegate{
    
    func initTabs() {
        viewContentTabs.dataSource = self
        viewContentTabs.orientation = .horizontal
        viewContentTabs.delegate = self
        viewNavigationTabs.viewPager = viewContentTabs
        // viewContentTabs.presentedPageIndex = 0
    }
    
    func bmoViewPagerDataSourceNaviagtionBarItemTitle(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> String? {
        return viewModel.arrayTabsNames[page]
    }
    
    func bmoViewPagerDataSourceNaviagtionBarItemNormalAttributed(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> [NSAttributedString.Key : Any]? {
        return [
            NSAttributedString.Key.font : UIFont(name: AppFont.instance.regular, size: 15)!,
            NSAttributedString.Key.foregroundColor : UIColor.fromHex(hex: "6D76AF")
        ]
    }
    func bmoViewPagerDataSourceNaviagtionBarItemHighlightedAttributed(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> [NSAttributedString.Key : Any]? {
        return [
            NSAttributedString.Key.font : UIFont(name: AppFont.instance.bold, size: 17)!,
            NSAttributedString.Key.foregroundColor : UIColor.white
        ]
    }
    
    func bmoViewPagerDataSourceNaviagtionBarItemHighlightedBackgroundView(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> UIView? {
        let view = UnderLineView()
        view.marginX = 0.0
        view.lineWidth = 5.0
        view.strokeColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        return view
    }
    
    // Required
    func bmoViewPagerDataSourceNumberOfPage(in viewPager: BmoViewPager) -> Int {
        return 2
    }
    
    func bmoViewPagerDataSourceNaviagtionBarItemSize(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> CGSize {
        return CGSize.init(width:  UIScreen.main.bounds.width/2, height: viewNavigationTabs.bounds.height
        )
    }
    
    func bmoViewPagerDataSource(_ viewPager: BmoViewPager, viewControllerForPageAt page: Int) -> UIViewController {
        return viewModel.arrarTabs[page]
    }
}
