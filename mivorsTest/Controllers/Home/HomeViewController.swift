//
//  HomeViewController.swift
//  mivorsTest
//
//  Created by jimmy on 2/23/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import UIKit
import BmoViewPager
import ImageSlideshow

class HomeViewController: UIViewController {
   
    @IBOutlet weak var imageSlider: ImageSlideshow!
    @IBOutlet weak var imageLeft: LeftImageView!
    @IBOutlet weak var imageRight: RightImageView!
    @IBOutlet weak var viewContentTabs: BmoViewPager!
    @IBOutlet weak var labelSwipTap: UILabel!
    @IBOutlet weak var labelSwipSelect: UILabel!
    var arrarTabs = [UIViewController]()
    var arrayImageView = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initObserver()
        initTabs()
        initGestures()
        initViewLanguage()
        setFonts()
        initSlider()
        MenuHandler.instance.initMenu(viewController: self)
        // Do any additional setup after loading the view.
    }

    private func initGestures(){
        imageRight.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedRight)))
        imageLeft.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedLeft)))
        viewContentTabs.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openPromotions)))
    }
    
    private func initObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(dismissNavigationController), name:NSNotification.Name.dismissNav, object: nil)
    }
    
    private func initViewLanguage(){
        labelSwipTap.text = SWIP_TAP_BRAND.localize
        labelSwipSelect.text = SWIP_SELECT_SECTION.localize
    }
    
    private func setFonts(){
        labelSwipSelect.setFont(name: AppFont.instance.regular, size: 16)
        labelSwipTap.setFont(name: AppFont.instance.regular, size: 16)
    }
    
    @objc private func dismissNavigationController(){
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @objc private func tappedRight(){
        if viewContentTabs.presentedPageIndex != 3{
            viewContentTabs.presentedPageIndex = viewContentTabs.presentedPageIndex + 1
        }
    }
    
   @objc private func tappedLeft(){
        if viewContentTabs.presentedPageIndex != 0{
            viewContentTabs.presentedPageIndex = viewContentTabs.presentedPageIndex - 1
        }
    }
    
    @objc private func openPromotions(){
        if viewContentTabs.presentedPageIndex == 0{
             navigationController?.pushViewController(PromotionViewController(), animated: true)
        }else if viewContentTabs.presentedPageIndex == 3{
            navigationController?.pushViewController(ServiceAppointmentViewController(), animated: true)
        }
       
    }
    
    @IBAction func openMenu(_ sender: Any) {
        MenuHandler.instance.openMenu(viewController: self)
    }

    @IBAction func tappedNext(_ sender: Any) {
        imageSlider.nextPage(animated: true)
    }
    
    @IBAction func tappedPrevious(_ sender: Any) {
        imageSlider.previousPage(animated: true)
    }
}
