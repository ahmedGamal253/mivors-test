//
//  HomeExtensions.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/24/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import Foundation
import BmoViewPager
import ImageSlideshow

extension HomeViewController: BmoViewPagerDataSource, BmoViewPagerDelegate{
    
    func initTabs() {
        viewContentTabs.dataSource = self
        viewContentTabs.orientation = .horizontal
        viewContentTabs.delegate = self
        arrarTabs.append(HomePagerViewController(image: #imageLiteral(resourceName: "Promotions_Icon"), number: "1/4", title:PROMOTIONS.localize))
        arrarTabs.append(HomePagerViewController(image: #imageLiteral(resourceName: "Wheel"), number: "2/4", title: ACCESSORIES.localize))
        arrarTabs.append(HomePagerViewController(image: #imageLiteral(resourceName: "Location Icon"), number: "3/4", title: BRANCHES.localize))
        arrarTabs.append(HomePagerViewController(image: #imageLiteral(resourceName: "Calender_Icon"), number: "4/4", title: SERVICE.localize))
    }
    
    func initSlider(){
        imageSlider.setImageInputs([
            ImageSource(image: #imageLiteral(resourceName: "Chevrolet_Logo")),
            ImageSource(image: #imageLiteral(resourceName: "kisspng-gac-group-trumpchi-car-saic-motor-auto-show-gac-motor-logo-5b3ed5a85369a4.9074907415308445843417")),
            ImageSource(image: #imageLiteral(resourceName: "GMC-logo-3800x1000"))
        ])
        imageSlider.circular = false
        let pageIndicator = UIPageControl()
        pageIndicator.currentPageIndicatorTintColor = UIColor.clear
        pageIndicator.pageIndicatorTintColor = UIColor.clear
        imageSlider.pageIndicator = pageIndicator
    }
    
    func bmoViewPagerDataSourceNumberOfPage(in viewPager: BmoViewPager) -> Int {
        return arrarTabs.count
      }
    
    func bmoViewPagerDataSource(_ viewPager: BmoViewPager, viewControllerForPageAt page: Int) -> UIViewController {
        return arrarTabs[page]
    }
}
