//
//  HomePagerViewController.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/24/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import UIKit

class HomePagerViewController: UIViewController {

    
    @IBOutlet weak var labelNumber: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageBranche: UIImageView!
    
    var imageItem = UIImage()
    var titleItem = ""
    var numberItem = ""
    
    init(image:UIImage,number:String,title:String) {
        super.init(nibName: "HomePagerViewController", bundle: nil)
        imageItem = image
        titleItem = title
        numberItem = number
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        // Do any additional setup after loading the view.
    }

    private func initView(){
        imageBranche.image = imageItem
        labelTitle.text = titleItem
        labelNumber.text = numberItem
        
    }
}
