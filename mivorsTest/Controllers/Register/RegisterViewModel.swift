//
//  RegisterViewModel.swift
//  mivorsTest
//
//  Created by jimmy on 2/24/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class RegisterViewModel{
    
    var showError = BehaviorRelay(value: "")
    var showLoader = BehaviorRelay(value: false)
    var sucsessRegister = BehaviorRelay(value: false)
    var openChooseType = BehaviorRelay(value: false)
    var hiddenGovernment = BehaviorRelay(value: true)
    var chooseTypeTitle = BehaviorRelay(value: CHOOSE_TYPE.localize)
    
    func tappedOpenChooseType(){
        if openChooseType.value == true{
            openChooseType.accept(false)
        }else{
            openChooseType.accept(true)
        }
    }
    
    func tappedChooseNew(){
        chooseTypeTitle.accept(NEW.localize)
        openChooseType.accept(false)
        hiddenGovernment.accept(true)
    }
    
    func tappedChooseExisting(){
        chooseTypeTitle.accept(EXISTING.localize)
        openChooseType.accept(false)
        hiddenGovernment.accept(false)
    }
    
    func TappedSignUp(name:String,email:String,mobile:String,password:String,confirmPassword:String,city:String,GID:String){
        if name.isEmpty{
            showError.accept("\(FULL_NAME.localize) \(REQUIRED.localize)")
        }else if email.isEmpty{
            showError.accept("\(EMAIL.localize) \(REQUIRED.localize)")
        }else if password.isEmpty{
            showError.accept("\(PASSWORD.localize) \(REQUIRED.localize)")
        }else if confirmPassword.isEmpty{
            showError.accept("\(confirmPassword.localize) \(REQUIRED.localize)")
        }else if password != confirmPassword{
            showError.accept("\(CONFIRM_PASSWORD.localize) \(REQUIRED.localize)")
        }else if mobile.isEmpty{
            showError.accept("\(MOBILE.localize) \(REQUIRED.localize)")
        }else if city.isEmpty{
            showError.accept("\(CITY.localize) \(REQUIRED.localize)")
        }else if chooseTypeTitle.value == EXISTING.localize{
            showError.accept("\(GOVERNMENT_ID.localize) \(REQUIRED.localize)")
        }else{
            signUp(name: name, email: email,mobile: mobile, password: password, confirmPassword: confirmPassword, city: city, GID: GID)
        }
    }
    
    private func signUp(name:String,email:String,mobile:String,password:String,confirmPassword:String,city:String,GID:String){
        showLoader.accept(true)
        let instance = AppConnectionsHandler()
        var params = [String:String]()
        params.updateValue(name, forKey: "FullName")
        params.updateValue(email, forKey: "Email")
        params.updateValue(mobile, forKey: "Mobile")
        params.updateValue(email, forKey: "Password")
        params.updateValue(name, forKey: "City")
        params.updateValue(email, forKey: "GID")
        instance.responseClosure = {(responseType: ResponseStatus, response: [String:Any]?)in
            self.showLoader.accept(false)
            switch responseType {
            case.sucess:
                UserDefaults.standard.setUserId(id: response!["ID"] as? String ?? "")
                self.sucsessRegister.accept(true)
                break
            case.errorFromServer:
                self.showError.accept(response!["Error"] as! String)
                break
            case.errorFromInternet:
                self.showError.accept(ERROR_INTERNET.localize)
                break
            }
            
        }
        instance.post(url: AppUrl.instance.register, params: params, headers: AppUtils.instance.getHeader())
    }
}
