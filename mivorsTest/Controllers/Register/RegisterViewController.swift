//
//  RegisterViewController.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/22/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var viewSignLeft: UIView!
    @IBOutlet weak var viewSignRight: UIView!
    @IBOutlet weak var viewContantChooses: UIView!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var textCity: LeftTextField!
    @IBOutlet weak var constrainBottomGovID: NSLayoutConstraint!
    @IBOutlet weak var constrainTopGovID: NSLayoutConstraint!
    @IBOutlet weak var constrainHightGovernmentID: NSLayoutConstraint!
    @IBOutlet weak var textGovernmentID: LeftTextField!
    @IBOutlet weak var textMobile: LeftTextField!
    @IBOutlet weak var textConfirmPassword: LeftTextField!
    @IBOutlet weak var textPassword: LeftTextField!
    @IBOutlet weak var textEmail: LeftTextField!
    @IBOutlet weak var textName: LeftTextField!
    @IBOutlet weak var labelExisting: LeftLBL!
    @IBOutlet weak var labelNew: LeftLBL!
    @IBOutlet weak var labelChooseType: LeftLBL!
    @IBOutlet weak var constrainHightViewNew: NSLayoutConstraint!
    
    @IBOutlet weak var labelLineChooseType: UILabel!
    @IBOutlet weak var constrainHightViewExisting: NSLayoutConstraint!
    @IBOutlet weak var viewExisting: UIView!
    @IBOutlet weak var viewNew: UIView!
    @IBOutlet weak var viewChooseType: UIView!
    let appFont = AppFont.instance
    let viewModel = RegisterViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initRx()
        initGestures()
        // Do any additional setup after loading the view.
    }
    
    private func initView(){
        DispatchQueue.main.async {
            self.initViewForLanguage()
            self.initFonts()
            self.initUi()
        }
    }
    
    private func initViewForLanguage(){
        labelChooseType.text = CHOOSE_TYPE.localize
        labelNew.text = NEW.localize
        labelExisting.text = EXISTING.localize
        textName.placeholder = FULL_NAME.localize
        textEmail.placeholder = EMAIL.localize
        textPassword.placeholder = PASSWORD.localize
        textConfirmPassword.placeholder = CONFIRM_PASSWORD.localize
        textMobile.placeholder = MOBILE.localize
        textGovernmentID.placeholder = GOVERNMENT_ID.localize
        textCity.placeholder = CITY.localize
        btnSignUp.setTitle(SIGN_UP.localize, for: .normal)
    }
    
    private func initFonts(){
        labelChooseType.setFont(name: appFont.bold, size: 16)
        labelNew.setFont(name: appFont.regular, size: 16)
        labelExisting.setFont(name: appFont.regular, size: 16)
        btnSignUp.setFont(name: appFont.bold, size: 17)
        textName.setFont(name: appFont.bold, size: 16)
        textEmail.setFont(name: appFont.bold, size: 16)
        textPassword.setFont(name: appFont.regular, size: 16)
        textConfirmPassword.setFont(name: appFont.regular, size: 16)
        textMobile.setFont(name: appFont.regular, size: 16)
        textGovernmentID.setFont(name: appFont.regular, size: 16)
        textCity.setFont(name: appFont.regular, size: 16)
    }
    
    private func initUi(){
        textName.placeHolderColor = #colorLiteral(red: 0.7058823529, green: 0.7450980392, blue: 1, alpha: 1)
        textConfirmPassword.placeHolderColor = #colorLiteral(red: 0.7058823529, green: 0.7450980392, blue: 1, alpha: 1)
        textEmail.placeHolderColor = #colorLiteral(red: 0.7058823529, green: 0.7450980392, blue: 1, alpha: 1)
        textPassword.placeHolderColor = #colorLiteral(red: 0.7058823529, green: 0.7450980392, blue: 1, alpha: 1)
        textMobile.placeHolderColor = #colorLiteral(red: 0.7058823529, green: 0.7450980392, blue: 1, alpha: 1)
        textGovernmentID.placeHolderColor = #colorLiteral(red: 0.7058823529, green: 0.7450980392, blue: 1, alpha: 1)
        textCity.placeHolderColor = #colorLiteral(red: 0.7058823529, green: 0.7450980392, blue: 1, alpha: 1)
        textEmail.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        textPassword.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        textName.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        textConfirmPassword.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        textMobile.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        textGovernmentID.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        textCity.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        viewContantChooses.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        viewSignLeft.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        viewSignRight.setBorder(color: #colorLiteral(red: 0.6039215686, green: 0.6784313725, blue: 1, alpha: 1), radius: 5, borderWidth: 1)
        btnSignUp.layer.cornerRadius = 5
    }
    
   private func initGestures(){
        viewChooseType.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedChooseType)))
        viewNew.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedChooseNew)))
        viewExisting.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedChooseExisting)))
    }
    
    private func initRx(){
         _ = viewModel.chooseTypeTitle.bind(to: labelChooseType.rx.text)
        _ = viewModel.showError.subscribe(onNext: { (error) in
            if error != ""{
                self.showAlart(message: error)
            }
        })
        _ = viewModel.showLoader.subscribe(onNext: { (show) in
            show ?  self.showIndicator() : self.hideIndicator()
        })
        _ = viewModel.hiddenGovernment.subscribe(onNext: { (hidden) in
            hidden ? self.hiddenGovernmentID() : self.showGovernmentID()
        })
        _ = viewModel.openChooseType.subscribe(onNext: { (open) in
            open ? self.openChooseType() : self.closeChooseType()
            self.animateView()
        })
        _ = viewModel.sucsessRegister.subscribe(onNext: { (success) in
            if success{
                self.navigationController?.pushViewController(HomeViewController(), animated: true)
            }
        })
    }
    
    @objc private func tappedChooseType(){
        viewModel.tappedOpenChooseType()
    }
    
    @objc private func tappedChooseNew(){
        viewModel.tappedChooseNew()
    }
    
    @objc private func tappedChooseExisting(){
        viewModel.tappedChooseExisting()
    }
    
    private func openChooseType(){
        constrainHightViewNew.constant = getRatio(50)
        constrainHightViewExisting.constant = getRatio(50)
        viewNew.isHidden = false
        viewExisting.isHidden = false
        labelLineChooseType.isHidden = false
    }
    
    private func closeChooseType(){
        constrainHightViewNew.constant = 0
        constrainHightViewExisting.constant = 0
        viewNew.isHidden = true
        viewExisting.isHidden = true
        labelLineChooseType.isHidden = true
    }
    
    private func hiddenGovernmentID(){
        textGovernmentID.isHidden = true
        constrainHightGovernmentID.constant = 0
        constrainTopGovID.constant = 0
        animateView()
    }
    
    private func showGovernmentID(){
        textGovernmentID.isHidden = false
        constrainHightGovernmentID.constant = getRatio(50)
        constrainTopGovID.constant = 20
        animateView()
    }
    
    
    @IBAction func signUp(_ sender: Any) {
        viewModel.TappedSignUp(name: textName.text!, email: textEmail.text!,mobile:textMobile.text!, password: textPassword.text!, confirmPassword: textConfirmPassword.text!, city: textCity.text!, GID: textGovernmentID.text!)
    }
}
