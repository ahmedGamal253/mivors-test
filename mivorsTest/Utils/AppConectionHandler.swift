//
//  AppConectionHandler.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/23/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import Foundation
import Foundation
import Alamofire

public enum ResponseStatus {
    case sucess
    case errorFromServer
    case errorFromInternet
}

public typealias ResponseClosure = (ResponseStatus, [String: Any]?) -> Void

class AppConnectionsHandler {
    
    var responseClosure:ResponseClosure?
    
    func get(url: String, headers: [String:String]? = nil) {
        if checkConnection() {
            Alamofire.request(url, method: HTTPMethod.get, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil {
                        self.sucess(response: response)
                    } else {
                        self.failInternnet()
                    }
                    break
                case .failure(_):
                    self.failInternnet()
                    break
                }
            }
        } else {
            self.failInternnet()
        }
    }
    
    func post(url: String, params: [String:String]? = nil, headers: [String:String]? = nil) {
        if checkConnection() {
            Alamofire.request(url, method: .post, parameters:params , encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil {
                        self.sucess(response: response)
                    } else {
                        self.failInternnet()
                    }
                    break
                case .failure(_):
                    self.failInternnet()
                    break
                }
            }
        } else {
            self.failInternnet()
        }
    }
    
    
    
    func checkConnection() -> Bool {
        let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
        print((reachabilityManager?.isReachable)!)
        return (reachabilityManager?.isReachable)!
    }
    
    private func sucess(response:DataResponse<Any>) {
        let dic = handle(dicc: (response.result.value! as? [String : Any] ?? [String: Any]()))
        if let status = dic["Status"] as? String {
            if status == "S" {
                responseClosure?(.sucess, dic)
            } else {
                responseClosure?(.errorFromServer, dic)
            }
        } else {
            responseClosure?(.errorFromServer, nil)
        }
    }
    
    private func failInternnet() {
        responseClosure?(.errorFromInternet, nil)
    }
    
    func handle(dicc: [String: Any]) -> [String: Any] {
        var dic = dicc
        for (key, value) in dic {
            if value is NSNull {
                dic[key] = "" as Any
            } else if value is Int {
                let temp : Int =  value as! Int
                dic[key] = String(temp)
            } else if value is Double {
                let temp : Double = value as! Double
                dic[key] = String(temp)
            }
        }
        return dic
    }
}
