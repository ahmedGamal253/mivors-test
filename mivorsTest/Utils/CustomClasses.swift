//
//  CustomClasses.swift
//  Notice
//
//  Created by Mahmoud Maamoun on 10/23/17.
//  Copyright © 2017 Mahmoud Maamoun. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher



// UIImageView
class RoundedImageView: UIImageView {
    override func awakeFromNib() {
        self.layer.cornerRadius = self.bounds.size.width / 2
        //self.layer.borderColor  = UIColor.color("#eeeeee")?.cgColor
        self.layer.borderWidth = 1.0
        self.clipsToBounds = true
    }
}

class RightImageView: UIImageView {
    override func awakeFromNib() {
        self.clipsToBounds = true
        self.contentMode = .scaleAspectFit
        if !(AppLanguageHandler.instance.IsEnglish()) {
            self.transform  = CGAffineTransform(scaleX: -1, y: 1)
        }else {
            
        }
    }
}

class LeftImageView: UIImageView {
    override func awakeFromNib() {
        self.clipsToBounds = true
        self.contentMode = .scaleAspectFit
        if !(AppLanguageHandler.instance.IsEnglish()) {
            self.transform  = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
}

class ImageViewFromUrl: UIImageView {
    override func awakeFromNib() {
        self.layer.cornerRadius = 8
      //  self.layer.borderWidth = 0.20
        self.clipsToBounds = true
    }
    func showImage(url:String) {
        let main = URL(string: url)
        self.kf.indicator?.startAnimatingView()
        self.kf.setImage(with: main)
    }
}

// UIButton
class RightBtn: UIButton {
    override func awakeFromNib() {
        self.clipsToBounds = true
        self.contentMode = .scaleAspectFit
        if !(AppLanguageHandler.instance.IsEnglish()) {
            self.transform  = CGAffineTransform(scaleX: -1, y: 1)
        }else {
            
        }
    }
}

class LeftBtn: UIButton {
    override func awakeFromNib() {
        self.clipsToBounds = true
        self.contentMode = .scaleAspectFit
        if !(AppLanguageHandler.instance.IsEnglish()) {
            self.transform  = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
}
// UITextField


class LeftTextField: UITextField {
    override func awakeFromNib() {
        if (AppLanguageHandler.instance.IsEnglish()) {
            self.textAlignment = .left
        }else {
            self.textAlignment = .right
            //  self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
}

class RightTextField: UITextField {
    override func awakeFromNib() {
        if (AppLanguageHandler.instance.IsEnglish()) {
            self.textAlignment = .right
        }else {
            self.textAlignment = .left
            // self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
}

// UILabel

class LeftLBL: UILabel {
    override func awakeFromNib() {
        self.clipsToBounds = true
        self.adjustsFontSizeToFitWidth = true
        if (AppLanguageHandler.instance.IsEnglish()) {
            self.textAlignment = .left
        }else {
            self.textAlignment = .right
            // self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
}

class RightLBL: UILabel {
    override func awakeFromNib() {
        self.clipsToBounds = true
        self.adjustsFontSizeToFitWidth = true
        if (AppLanguageHandler.instance.IsEnglish()) {
            self.textAlignment = .right
        }else {
            self.textAlignment = .left
            //  self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
}
class CenterLBL: UILabel {
    override func awakeFromNib() {
        self.clipsToBounds = true
        if (AppLanguageHandler.instance.IsEnglish()) {
            self.textAlignment = .center
        }else {
            self.textAlignment = .center
            // self.transform  = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
}















