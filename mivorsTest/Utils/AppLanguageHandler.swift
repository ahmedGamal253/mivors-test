//
//  AppLanguageHandler.swift
//  Ellaithy
//
//  Created by Mohamed Elmaazy on 2/19/18.
//  Copyright © 2018 Mohamed Elmaazy. All rights reserved.
//

import Foundation
class AppLanguageHandler {
    
    static let instance = AppLanguageHandler()
//    private init() { }
    
    func getLanguageDic() -> NSDictionary {
        if (UserDefaults.standard.string(forKey: LANGUAGE_KEY) == ARABIC) {
            let path = Bundle.main.path(forResource:"ArabicList", ofType: "plist")
            return NSDictionary(contentsOfFile: path!)!
        } else   {
            let path = Bundle.main.path(forResource:"EnglishList", ofType: "plist")
            return NSDictionary(contentsOfFile: path!)!
        }
    }
    
    func getStringForKey(key:String) -> String {
        let dic = getLanguageDic()
        return dic.object(forKey: key) as? String ?? ""
    }
    
    func getLanguage() -> String {
        if (UserDefaults.standard.string(forKey: LANGUAGE_KEY) == ARABIC) {
            return ARABIC
        } else if (UserDefaults.standard.string(forKey: LANGUAGE_KEY) == ENGLISH) {
            return ENGLISH
        } else {
            return ""
        }
    }
    
    func setLanguage(language:String) {
        UserDefaults.standard.set(language, forKey: LANGUAGE_KEY)
    }
    
    func getLocalize()->String{
        if getLanguage() == ARABIC {
            return "1"
        }else{
            return "2"
        }
    }
    
    func swapeLanguages() {
        if getLanguage() == ARABIC {
            setLanguage(language: ENGLISH)
        } else {
            setLanguage(language: ARABIC)
        }
    }
    
    func IsEnglish() -> Bool {
           if (UserDefaults.standard.string(forKey:LANGUAGE_KEY) == ARABIC) {
               return false
           } else {
               return true
           }
       }
}
