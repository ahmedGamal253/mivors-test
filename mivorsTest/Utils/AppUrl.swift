//
//  AppUrl.swift
//  Yacoun
//
//  Created by User on 11/6/17.
//  Copyright © 2017 Internet Plus. All rights reserved.
//

import Foundation

class AppUrl {
    
    static let instance = AppUrl()
    private init() { }
    
    static let baseUrl = "http://mivorstest-env.hfzgkrfnk8.us-east-2.elasticbeanstalk.com/Api/"
    let register = "\(AppUrl.baseUrl)Register"
    let login = "\(AppUrl.baseUrl)Login"
    let promotions = "\(AppUrl.baseUrl)Promotion"
}

