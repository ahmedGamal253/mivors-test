//
//  NotificationNameExtensions.swift
//  mivorsTest
//
//  Created by Ahmed Gamal on 2/23/20.
//  Copyright © 2020 Ahmed Gamal. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let dismissNav = Notification.Name("dismissNavController")
    static let openDetails = Notification.Name("openDetails")
}
