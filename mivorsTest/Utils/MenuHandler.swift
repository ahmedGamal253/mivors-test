//
//  MenuHandler.swift
//  1Pharmacy
//
//  Created by mohamed elmaazy on 5/31/18.
//  Copyright © 2018 mohamed elmaazy. All rights reserved.
//

import Foundation
import SideMenu

class MenuHandler {
    
    static let instance = MenuHandler()
    private init () {}
    
    func initMenu(viewController:UIViewController) {
        let nav = UISideMenuNavigationController.init(rootViewController: MenuViewController())
        nav.isNavigationBarHidden = true
        if AppLanguageHandler.instance.IsEnglish(){
            SideMenuManager.default.menuLeftNavigationController = nav
            SideMenuManager.default.menuRightNavigationController = nil
        }else{
            SideMenuManager.default.menuLeftNavigationController = nil
            SideMenuManager.default.menuRightNavigationController = nav
        }
        SideMenuManager.default.menuAddPanGestureToPresent(toView: viewController.view)
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuAnimationFadeStrength = CGFloat(0.1)
        SideMenuManager.default.menuWidth = UIScreen.main.bounds.width * 304 / 360
    }
    
    func openMenu(viewController:UIViewController) {
        if AppLanguageHandler.instance.IsEnglish(){
            viewController.present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
        }else{
            viewController.present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
        }
    }
    
}
